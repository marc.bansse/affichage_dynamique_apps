# affichage_dynamique_apps_se4



## Présentation
Ce script va permettre de transformer une Debian 12 Bookworm sans interface graphique en un client d'affichage dynamique. Un diaporama accéssible par lien direct (par exemple une présentation stockée sur APPS (https://nuage03.apps.education.fr par exemple) et donc modifiable par plusieurs personnes) sera lu en boucle par Libreoffice Impress en mode plein écran.

On utilise donc ici aucun GAFAM ou autre plateforme opaque qui va collecter des données personnelles.

Le système télécharge régulièrement le diaporama. En cas de modification par quelqu'un, le diaporama est à nouveau chargé et relancé.


## Mise en place

On installe une Debian 12 sans interface graphique, mais avec les utilitaires usuels du système. Il sera préférable de mettre le poste en Ip fixe pour l'administration à distance. Si on a désactivé les ports USB du poste, l'accès SSH sera le seul moyen de modifier les paramètres.

Il suffit de télécharger le script

```
wget https://forge.apps.education.fr/marc.bansse/affichage_dynamique_apps/-/raw/main/install_kiosk.sh
chmod u+x install_kiosk.sh
```
En cas de problème de proxy, il faut ajouter les paramètres de l'AMON dans /etc/wgetrc


```
http_proxy = http://10.0.0.1:3128
https_proxy = http://10.0.0.1:3128
use_proxy = on
```
On peut éditer directement le script pour indiquer les paramètres en début de script.

L'adresse externe d'un diaporama apps peut être obtenue directement par une simple clic droit sur l'adresse publique de partage du diaporama.

![adresse externe](image.png)


On peut évidemment utiliser un autre stockage qu'apps, tout diaporama accéssible par un lien publique direct sera utilisable.

On lance le script en root.
```
./install_kiosk.sh
```

Tous les paquets vont s'installer sur le poste, l'utilisateur kiosk va être créé et le fichier autostart contenant toutes les informations va également être généré.

## Principe de fonctionnement.

On fabrique un fichier diaporama sur APPS (https://nuage03.apps.education.fr à adapyter selon son numéro de nuage). On met ce fichier en partage avec droits d'écriture pour les utilisateurs désirés.

Chaque personne peut modifier le fichier dans le répertoire de synchronisation du client Nextcloud avec son utilitaire favori (Libreoffice,Onlyoffice,Powerpoint ). On peut aussi utiliser la version online de nuage.apps.

Le script est téléchargé toutes les 5 minutes (la durée peut se changer dans le fichier autostart). Dès que le contenu du fichier est modifié, Libreoffice se ferme et se relance avec le nouveau contenu.

ATTENTION: Le fichier de présentation doit avoir des transitions automatiques (menu Diapo puis transitions), et être réglé pour se lancer en boucle (Menu Diaporama puis paramètres du Diaporama).

![Transition automatique](image-1.png)

Dans la partie "paramètres du diaporama:

![paramètres diaporama](image-2.png)


## Administration du poste.
Le fichier _/home/kiosk/.config/openbox/autostart_ possède des variables à éditer:

_url_diapo_ qui doit donc contenir l'adresse externe du diaporama.

_url_wallpaper_ qui doit contenir l'adresse externe du fond d'écran qui s'affichera en tout début d'allumage, ou lorsque le diaporama sera relancé.

_time_refresh_ correspond à la durée entre deux vérifications de modifications. Par défaut le diaporama est donc téléchargé toutes les 5 minutes (300s). Le délais peut être rallongé si l'édition du diaporama est peu fréquente.

**Il faudra reboot le poste pour que les modifications soient prises en compte.**



Ce script est clairement inspiré par ces 2 excellents articles:

https://github.com/josfaber/debian-kiosk-installer/blob/master/kiosk-installer.sh

https://web.archive.org/web/20190312163415/http://wiki.dane.ac-versailles.fr/index.php?title=Affichage_dynamique_avec_Raspberry_et_LibreOffice_Impress,_dans_un_r%C3%A9seau_Samba




