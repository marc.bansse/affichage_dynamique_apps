#!/bin/bash


#variables à indiquer ici
proxy_ip="10.0.0.1"
proxy_port="3128"
#En cas de proxy mettre yes dans la variable use_proxy
use_proxy="yes"

#Variables du diaporama qui seront indiquées dans le fichier /home/kiosk/.config/openbox/autostart

url_diapo="https://nuage03.apps.education.fr/index.php/s/XXXXXXXXXXXXXXX/download/affichage_sdp.pptx"
url_wallpaper="https://nuage03.apps.education.fr/index.php/s/6wEqFjAsdEf7Ard/download/wallpaper_se.jpeg"
wallpaper_file="wallpaper.jpg"
chemin_diapo="/home/kiosk"
nom_diapo="diapo"
nom_diapo_temp="diapo_temp"
pid=""
time_refresh="300"
sha256sum_temp=""
sha256sum_actuel=""



install_paquets()
{
# mise a jour du systeme
apt update

# installation des logiciels
apt install -y \
    unclutter \
    xorg \
    chromium \
    openbox \
    lightdm \
    locales \
    xdotool \
    xscreensaver \
    feh \
    libreoffice 
 #   libreoffice-avmedia-backend-gstreamer
}

mise_en_place_poweroff()
{
# On ajoute dans le cron l'extinction du poste à 19h tous les jours.
# Pour changer cette heure/ taper crontab -e en root sur le poste.
echo "00 19 * * * /sbin/poweroff" >> /var/spool/cron/crontabs/root
}


disable_usb_ports()
{
echo '1-1' | tee /sys/bus/usb/drivers/usb/unbind
# Pour réactiver les ports usb il faut lancer:
# echo '1-1' | sudo tee /sys/bus/usb/drivers/usb/bind
}


install_user_kiosk()
{
# dir
mkdir -p /home/kiosk/.config/openbox

# creation du groupe
groupadd kiosk

# creation de l'utilisateur s'il n'existe pas
id -u kiosk &>/dev/null || useradd -m kiosk -g kiosk -s /bin/bash

# Droits de l'utilisateur
chown -R kiosk:kiosk /home/kiosk
}

install_autologin()
{

# retirer les anciennes consoles
if [ -e "/etc/X11/xorg.conf" ]; then
  mv /etc/X11/xorg.conf /etc/X11/xorg.conf.backup
fi
cat > /etc/X11/xorg.conf << EOF
Section "ServerFlags"
    Option "DontVTSwitch" "true"
EndSection
EOF

# creation du fichier de configuration
if [ -e "/etc/lightdm/lightdm.conf" ]; then
  mv /etc/lightdm/lightdm.conf /etc/lightdm/lightdm.conf.backup
fi
cat > /etc/lightdm/lightdm.conf << EOF
[SeatDefaults]
autologin-user=kiosk
user-session=openbox
EOF

}


reglage_proxy_wget()
{
if [ "$use_proxy" = "yes" ]
then
cat >> /etc/wgetrc << EOF
http_proxy = http://$proxy_ip:$proxy_port
https_proxy = http://$proxy_ip:$proxy_port
use_proxy = on
EOF
fi

}

create_autostart()
{
# create autostart
if [ -e "/home/kiosk/.config/openbox/autostart" ]; then
  mv -f /home/kiosk/.config/openbox/autostart /home/kiosk/.config/openbox/autostart.backup
fi
cat > /home/kiosk/.config/openbox/autostart << EOF

#!/bin/bash

#variables à initialiser
url_diapo="$url_diapo"
url_wallpaper="$url_wallpaper"
wallpaper_file="$wallpaper_file"
chemin_diapo="$chemin_diapo"
nom_diapo="$nom_diapo"
nom_diapo_temp="$nom_diapo_temp"
time_refresh="$time_refresh"
pid=""
sha256sum_temp=""
sha256sum_actuel=""

wallpaper()
{
  if [ -f "\$chemin_diapo/\$wallpaper_file" ]
    then
        feh -Z -F -Y "\$chemin_diapo/\$wallpaper_file" &
    else
        wget -P "\$chemin_diapo" -O "\$chemin_diapo/\$wallpaper_file" -q "\$url_wallpaper"
        feh -Z -F -Y "\$chemin_diapo/\$wallpaper_file" &
    fi
}



recup_diapo()
{
#On supprime le fichier qui est sur le serveur et probablement obsolète
rm -f "\${chemin_diapo}/\${nom_diapo_temp}"

wget -P "\$chemin_diapo" -O "\$chemin_diapo/\$nom_diapo_temp" -q "\$url_diapo"
sha256sum_temp=\$(sha256sum \$chemin_diapo/\$nom_diapo_temp |cut -d " " -f1 )
#echo "$sha256sum_temp"
}

premier_lancement()
{
#on indique que le sha256sum de départ est celui de 
sha256sum_actuel="\$sha256sum_temp"
#On modifie le nom de fichier temporaire pour indiquer qu'il s'agit du bon fichier lancé 
mv -f "\$chemin_diapo/\$nom_diapo_temp" "\$chemin_diapo/\$nom_diapo"

#On lance le diaporama avec la version tout de suite récuperée sur le web
unclutter -idle 0.1 -grab -root &

xscreensaver -no-splash & #permet de bloquer ecran de veille

xset s off & #desactive l'economiseur d'ecran
xset -dpms & #desactive le mode veille 
xset s noblank & #empeche d'activer un ecran noir

xrandr --auto
libreoffice --norestore --show "\$chemin_diapo/\$nom_diapo" & PID=\$(echo "\$!")
sleep 12
xscreensaver-command -deactivate

}

refresh_diapo()
{


while true; do
        recup_diapo


        if [ "\$sha256sum_actuel" != "\$sha256sum_temp" ]
        then
                # On ferme le diapo projeté
                kill "\$PID"

                # On attend 2 secondes avant de passer à la suite
                sleep 2

                #On supprime l'ancien fichier
                rm -f "\$chemin_diapo/\$nom_diapo"

                #On met le nouveau fichier
                mv -f "\$chemin_diapo/\$nom_diapo_temp" "\$chemin_diapo/\$nom_diapo"

                sleep 10
                sha256sum_actuel="\$sha256sum_temp"
                libreoffice --norestore --show "\$chemin_diapo/\$nom_diapo" & PID=\$(echo "\$!")
                sleep 12
		            xscreensaver-command -deactivate

        else
        sleep "\$time_refresh"
        fi
done
}

#le script!
wallpaper
recup_diapo
premier_lancement
refresh_diapo

EOF

echo "Done!"

}





#le script
install_paquets
install_user_kiosk
mise_en_place_poweroff
#disable_usb_ports
install_autologin
reglage_proxy_wget
create_autostart